//
//  MyUIViewController.swift
//  HW-002-Sign up form
//
//  Created by Ratanak Phang on 11/24/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

class MyUIViewController: UIViewController{

    @IBOutlet weak var showLabel: UILabel!
    var username: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        showLabel.textColor = UIColor.brown
        showLabel.text = "Welcome \(String(username!).uppercased()), your account has been created!"

    }
    
}
