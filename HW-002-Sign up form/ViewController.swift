//
//  ViewController.swift
//  HW-002-Sign up form
//
//  Created by Ratanak Phang on 11/23/17.
//  Copyright © 2017 Ratanak Phang. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    let fbColor: UIColor = UIColor(displayP3Red: 73/255, green: 104/255, blue: 173/255, alpha: 1)
    let myFont: UIFont = UIFont.boldSystemFont(ofSize: 20)
    let myBorder = UITextBorderStyle.roundedRect
    
    var myInt = Int()
    
    @IBOutlet weak var createYourAccount: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var contactPhoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var signUpButton: UIButton!
    
    let khmerAreaCode: UITextField = UITextField(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 71, height: 45)))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = fbColor
        
        //custom text field
        khmerAreaCode.isEnabled = false
        self.contactPhoneTextField.leftView = khmerAreaCode
        self.khmerAreaCode.textColor = UIColor.black
        self.khmerAreaCode.isHidden = true
        self.contactPhoneTextField.leftViewMode = .never
        khmerAreaCode.font = UIFont.boldSystemFont(ofSize: 20)
        
        
        createYourAccount.textColor = UIColor.white
        createYourAccount.font = UIFont.boldSystemFont(ofSize: 30)
        
        descriptionLabel.textColor = UIColor.white
        descriptionLabel.font = UIFont.systemFont(ofSize: 13)
        descriptionLabel.text = "By signing up your account, you will agree our term"
        descriptionLabel.textAlignment = .center
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.numberOfLines = 1
        
        // Custome username text field
        usernameTextField.borderStyle = myBorder
        usernameTextField.backgroundColor = UIColor.white
        usernameTextField.placeholder = "Username"
        usernameTextField.font = myFont
        
        // Custom password text field
        passwordTextField.borderStyle = myBorder
        passwordTextField.backgroundColor = UIColor.white
        passwordTextField.placeholder = "Password"
        passwordTextField.isSecureTextEntry = true
        passwordTextField.font = myFont
        
        // Custom Contact Phone Text Field
        contactPhoneTextField.borderStyle = myBorder
        contactPhoneTextField.backgroundColor = UIColor.white
        contactPhoneTextField.placeholder = "(+855) 096-123-4567"
        contactPhoneTextField.font = myFont
        
        //Custom email phone Text Field
        emailTextField.borderStyle = myBorder
        emailTextField.backgroundColor = UIColor.white
        emailTextField.placeholder = "example@gmail.com"
        emailTextField.font = myFont
        
        //Custom sign up button
        signUpButton.backgroundColor = UIColor(red: 0x8B/255, green: 0xD4/255, blue: 0xFB/255, alpha: 1)
        
        
        self.usernameTextField.delegate = self
        self.usernameTextField.tag = 1
        self.usernameTextField.returnKeyType = UIReturnKeyType.next
        
        self.passwordTextField.delegate = self
        self.passwordTextField.tag = 2
        self.passwordTextField.returnKeyType = UIReturnKeyType.next
        
        self.contactPhoneTextField.delegate = self
        self.contactPhoneTextField.tag = 3
        
        self.emailTextField.delegate = self
        self.emailTextField.tag = 4
        self.emailTextField.returnKeyType = UIReturnKeyType.done
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.usernameTextField.resignFirstResponder()
//        self.passwordTextField.resignFirstResponder()
//        self.contactPhoneTextField.resignFirstResponder()
//        self.emailTextField.resignFirstResponder()
        
        let nextTag = textField.tag + 1
        let nextResponse = textField.superview?.viewWithTag(nextTag) as UIResponder!
        
        if nextResponse != nil {
            nextResponse?.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        usernameTextField.keyboardType = UIKeyboardType.default
        passwordTextField.keyboardType = UIKeyboardType.alphabet
        contactPhoneTextField.keyboardType = UIKeyboardType.phonePad
        emailTextField.keyboardType = UIKeyboardType.emailAddress
        
        if textField.tag == 3 {
            contactPhoneTextField.clearsOnInsertion = false
            contactPhoneTextField.placeholder = "096-123-4567"
            khmerAreaCode.isHidden = false
            contactPhoneTextField.leftViewMode = .always
            khmerAreaCode.text = " (+855)"
            khmerAreaCode.textColor = UIColor.black
        }
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 3 {
            if textField.text?.count == 0 {
                contactPhoneTextField.placeholder = "(+855) 096-123-4567"
                khmerAreaCode.isHidden = true
                contactPhoneTextField.leftViewMode = .never
            }
        }
        
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 3 {
            
            /*var groupSize = 2
            let separator = "-"
            if string.count == 0 {
                groupSize = 4
            }
            let formatter = NumberFormatter()
            formatter.groupingSeparator = separator
            formatter.groupingSize = groupSize
            formatter.usesGroupingSeparator = true
            formatter.secondaryGroupingSize = 2
            if var number = textField.text, string != "" {
                number = number.replacingOccurrences(of: separator, with: "")
                if let doubleVal = Double(number) {
                    let requiredString = formatter.string(from: NSNumber.init(value: doubleVal))
                    textField.text = requiredString
                }
            }*/
           // @objc func formattedNumber(_ textField: UITextField) {
            if textField == contactPhoneTextField {
                let phone = contactPhoneTextField.text!
                let cleanPhoneNumber = phone.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
                var mask = "XXX-XXX-XXXX"
                
                var result = ""
                var index = cleanPhoneNumber.startIndex
                for ch in mask.characters {
                    if index == cleanPhoneNumber.endIndex {
                        break
                    }
                    if ch == "X" {
                        result.append(cleanPhoneNumber[index])
                        index = cleanPhoneNumber.index(after: index)
                    } else {
                        result.append(ch)
                    }
                }
                textField.text = result
            }
            
           // }

            
            guard let text = textField.text else {
                return true
            }
            
            let newLength = text.utf16.count + string.utf16.count - range.length
            return newLength <= 12 // Bool
        }
        
        
        
        
        
        
        
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        usernameTextField.becomeFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationViewController: MyUIViewController = segue.destination as! MyUIViewController
        destinationViewController.username = self.usernameTextField.text
    }
    
    
    @IBAction func unwindSegue(_ sender: UIStoryboardSegue) {
        self.usernameTextField.text = nil
        self.passwordTextField.text = nil
        self.contactPhoneTextField.text = nil
        self.emailTextField.text = nil
        
    }
    
    
    @IBAction func signUpButtonAction(_ sender: UIButton) {
       // self.shouldPerformSegue(withIdentifier: "mySegue", sender: )
    }
    

    @IBAction func contactPhoneTextFieldAction(_ sender: UITextField) {
        let textLenght = sender.text?.count
        print(sender.text!)
        print(textLenght!)
    
    }
    
    
   
    
    
   // contactPhoneTextFieldAction

}












